import sys
from csv import reader
import numpy as np

name = sys.argv[1]
open_ = []
high = []
low = []
close = []
volume = []
adj_close = []
csv = reader(open(name, 'rb'), delimiter=',')
i = 0
first = ""
last = ""
for row in csv:
    i += 1
    if(row[0] != 'Date'):
        if(i==2):
            first = row[0]
        else:
            last = row[0]
        open_.append(float(row[1]))
        high.append(float(row[2]))
        low.append(float(row[3]))
        close.append(float(row[4]))
        volume.append(float(row[5]))
        adj_close.append(float(row[6]))
del csv
np.savez(name[:-4] , open_=np.array(open_), high=np.array(high), low=np.array(low), close=np.array(close), volume=np.array(volume), adj_close=np.array(adj_close))
print i
print first
print last 
