# -*- coding: utf-8 -*-
"""
Created on Sat Oct  3 18:38:55 2015

@author: vikweg
"""
import matplotlib.pyplot as plt
from csv import reader
import numpy as np

import getMentions

def plot(name):

    open_ = []
    high = []
    low = []
    close = []
    volume = []
    adj_close = []
    csv = reader(open(name, 'r'), delimiter=',')
    i = 0
    first = ""
    last = ""
    for row in csv:
        i += 1
        if(row[0] != 'Date'):
            if(i==2):
                first = row[0]
            else:
                last = row[0]
            open_.append(float(row[1]))
            high.append(float(row[2]))
            low.append(float(row[3]))
            close.append(float(row[4]))
            volume.append(float(row[5]))
            adj_close.append(float(row[6]))
    del csv
    
    m = getMentions.getMentions(0)
    
    ret = np.divide(np.subtract(open_[2:],open_[:-2]),open_[:-2])
    
    plt.plot(range(len(open_)),np.divide(open_,max(open_)),range(len(open_)),np.divide(m,max(m)),range(len(ret)),ret)
    
plot('C:/ETH/projects_small/hackzurich15/timeseries/157_EBAY.csv')