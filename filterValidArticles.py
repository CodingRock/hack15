## Spark Application - execute with spark-submit

## Imports
import csv
import getWords
import getDate

from StringIO import StringIO
from pyspark import SparkConf, SparkContext

ARTICLE_TYPE = 2

SAMPLE_DATA = 0
PATH = "/user/root/DataSource2011-2014"
FIRST_DATE = "2011-01-03"
if(SAMPLE_DATA):
    PATH = "/data/DataSource2004-2010"
    FIRST_DATE = "2003-12-31"
COMPANY_SYMBOL = "GOOGL"

words = ['Inc.', 'Inc', 'Co.', 'Corp.', 'Corporation', ',', 'Holdings', 'Ltd.', 'Ltd', '(Hldg. )']

def name(tuple):
    return [tuple[0], len(tuple[1])]

## Closure Functions
def parseCompany(row):
    """
    Parses a row and returns a named tuple.
    """
    for w in words:
        row[2] = row[2].replace(w, '')
    return [row[1].lower(), row[2].lower()]

def split(line):
    """
    Operator function for splitting a line with csv module
    """
    reader = csv.reader(StringIO(line), delimiter=';')
    return reader.next()

def findarticles(stock,article):
    return ((stock[0] in article[1].lower()) or (stock[1] in article[1].lower()))
#    return ((stock[0] in article[1]) or (stock[1] in article[1]) or (stock[2] in article[1]) or (stock[3] in article[1]))

def findSEC(stock,article):
    return (stock in article[0])
def na(article):
    return article[0]

## Main functionality
def main(sc):

    # Load the airlines lookup dictionary
    stocks = sc.textFile('hdfs://'+PATH+'/Metadata/MetaData_SP500.csv').filter(lambda s: s[0:2] != 'id').map(split).map(parseCompany)
    tmp = dict(stocks.collect())
    stocks_lookup = sc.broadcast(tmp)
    gD = lambda a: getDate.getDates(a,FIRST_DATE)
    ebay = 0
    stock_ar = [' '+stocks[COMPANY_SYMBOL]+' ', ' '+COMPANY_SYMBOL+' ', ' '+stocks[COMPANY_SYMBOL]+"'s", " GOOGLE's"] 
    print stock_ar
    if ARTICLE_TYPE == 0: 
        article = sc.wholeTextFiles('hdfs://'+PATH+'/Bloomberg/')#.map(getWords)
        ebay = article.filter(lambda a: findarticles(stock_ar,a)).map(gD).collect() 
    elif ARTICLE_TYPE == 1:
        article = sc.wholeTextFiles('hdfs://'+PATH+'/SEC/*.html')#.map(getWords)
        ebay = article.filter(lambda a: findSEC('GOOGL',a)).map(name).collect()    
    elif ARTICLE_TYPE == 2: 
        article = sc.wholeTextFiles('hdfs://'+PATH+'/Telegraph/')#.map(getWords)
        ebay = article.filter(lambda a: findarticles(stock_ar,a)).map(gD).collect() 
    elif ARTICLE_TYPE == 3:
        article = sc.wholeTextFiles('hdfs://'+PATH+'/FTC/*.html')#.map(getWords)
        ebay = article.filter(lambda a: findSEC('google',a)).map(gD).collect()    
    print(ebay)

if __name__ == "__main__":

    # Configure Spark
    conf = SparkConf().setMaster("local[*]")
    conf = conf.setAppName('blubb')
    sc   = SparkContext(conf=conf)
    # Execute Main functionality
    main(sc)
