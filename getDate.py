#import codecs 
import datetime
def getDates(tuple, compareTime):
	text = tuple[1]
	date2 = datetime.datetime.strptime(compareTime, "%Y-%m-%d")
	lines = text.split('\n')
	for i in range(0, len(lines)):
		if 'datetime=\'' in lines[i]:
			# Bloomberg: <time class='byline-text' datetime='2008-08-25T20:00:00-04:00' itemprop='datePublished'>
			try:
				split1 = lines[i].split('datetime=\'')
				split2 = split1[1].split('T')
				date = datetime.datetime.strptime(split2[0], "%Y-%m-%d")
				return abs((date - date2).days)
			except:
				return -1
		elif 'datetime="' in lines[i]:
			# FTC: <time datetime="2009-10-30 04:00:00" class="field field-name-field-date field-type-datetime field-label-hidden field-wrapper">
			try:
				split1 = lines[i].split('datetime="')
				split2 = split1[1].split(' ')
				date = datetime.datetime.strptime(split2[0], "%Y-%m-%d")
				return abs((date - date2).days)
			except:
				return -1
		elif 'tmglLasUpdatedDateFeed' in lines[i]:
			# Telegraph: <div id="tmglLasUpdatedDateFeed">
			# <p>
			# Thursday 27 August 2015</p>
			try:
				split1 = lines[i+2].lstrip().split('p')
				date = datetime.datetime.strptime(split1[0], "%A %d %B %Y")
				return abs((date - date2).days)
			except:
				try:
					split1 = lines[i+2].lstrip().split('<')
					date = datetime.datetime.strptime(split1[0], "%A %d %B %Y")
					return abs((date - date2).days)
				except:
					return -1
		elif 'Date:' in lines[i]:
			# SEC: Date: August 4, 2006&nbsp;
			try:
				split1 = lines[i].split('Date:')
				split2 = split1[1].lstrip().split('&nbsp;')
				date = datetime.datetime.strptime(split2[0], "%B %d, %Y")
				return abs((date - date2).days)
			except:
				return -1
			
#with codecs.open('data/1.txt', 'r', encoding='utf8', errors='ignore') as file1:
#	print(getDates(file1.read(), '2015-10-03'))
#with codecs.open('data/2.txt', 'r', encoding='utf8', errors='ignore') as file2:
#	print(getDates(file2.read(), '2015-10-03'))
#with codecs.open('data/3.txt', 'r', encoding='utf8', errors='ignore') as file3:
#	print(getDates(file3.read(), '2015-10-03'))
#with codecs.open('data/3-1.txt', 'r', encoding='utf8', errors='ignore') as file3_1:
#	print(getDates(file3_1.read(), '2015-10-03'))
#with codecs.open('data/4.txt', 'r', encoding='utf8', errors='ignore') as file4:
#	print(getDates(file4.read(), '2015-10-03'))
#with codecs.open('data/4-1.txt', 'r', encoding='utf8', errors='ignore') as file4_1:
#	print(getDates(file4_1.read(), '2015-10-03'))