import sys
def getWords(data): #data = filename and string
	# Split data into lines
    lines = data[1].split("\n")
    text = []
    textReading = 0
    for i in range(0, len(lines)):
        # Extract headlines
        if "class='headline'" in lines[i] or "itemprop=\"headline name\"" in lines[i] or "itemprop=\"alternativeHeadline description\"" in lines[i] or "page-title" in lines[i] or "field-subtitle" in lines[i]:
            # Headline Bloomberg & Telegraph
            lines[i] = stripStartTag(lines[i])
            while("<" not in lines[i]):
                text += stripTextLine(lines[i])
                i += 1
            lines[i] = stripEndTag(lines[i])
            text += stripTextLine(lines[i])
        # Detect end of main content
        if textReading > 0:
            if textReading == 1 and "</div>" in lines[i]:
                textReading = 0
                break
            if textReading == 2 and "<div class=\"cl\"></div>" in lines[i]:
                textReading = 0
                text += stripTextLine(lines[i]) #Telegraph has the content in one single line
                break
            if textReading == 3 and "" in lines[i]:
                textReading = 0
                break
            text += stripTextLine(lines[i])
        #Detect starts of main content
        if "id='article_body'" in lines[i]:
            #Text starts from now
            textReading = 1
        if "itemprop=\"articleBody\"" in lines[i]:
            #Text starts from now
            textReading = 2
        if "content:encoded" in lines[i]:
            #Text starts from now
            textReading = 3
    return text

def stripTextLine(line):
    # Remove all html tags
    line = line.replace("\\<.*?>", "")
    lines = line.split(" ")
    # Discard any non-alphanumeric and change to lower case
    for line in lines:
        line.replace("\\W", "").lower()
        line += " "
    return lines

def stripStartTag(line):
    lines = line.split(">")
    if len(lines) >= 2:
        return lines[1]
    else:
        return ""

def stripEndTag(line):
    return line.split("<")[0]

#print(getWords((1, str)))