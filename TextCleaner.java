import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

public class TextCleaner {

	public static void main(String[] args) {

		List<String> fileNames = new LinkedList<String>();

		if (args.length == 0) {
			// Scan entire directory
			File dir = new File(".");
			File[] filesList = dir.listFiles();
			for (int i = 0; i < filesList.length; i++) {
				if (!filesList[i].isDirectory()
						&& TextCleaner.getFileExtension(filesList[i].getName()).equals("html")) {
					fileNames.add(filesList[i].getName());
				}
			}
		} else {
			for (int i = 0; i < args.length; i++) {
				fileNames.add(args[i]);
			}
		}

		// Parse all files given as arguments
		for (int i = 0; i < fileNames.size(); i++) {
			String text = extractText(fileNames.get(i)), commonWords = null;

			// System.out.println(text + "\n");
			// PrintWriter out = null;
			// try {
			// String newName = fileNames.get(i) + ".txt";
			// out = new PrintWriter(newName);
			// out.println(text);
			// System.out.println("Created file: " + newName);
			// } catch (FileNotFoundException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } finally {
			// out.close();
			// }
			// System.out.println(fileNames.get(i) + ": " + text);
			// try {
			// // Read data
			// File inputFile = new File("commonWords.txt");
			// FileInputStream fis = new FileInputStream(inputFile);
			// byte[] data = new byte[(int) inputFile.length()];
			// fis.read(data);
			// fis.close();
			// commonWords = new String(data, "UTF-8");
			// } catch (FileNotFoundException e) {
			// e.printStackTrace();
			// } catch (IOException e) {
			// e.printStackTrace();
			// }
			// Map<String, Integer> map = new HashMap<>();
			// map = frequency(text, commonWords);
			//
			// output(map);
		}
	}

	public static String getFileExtension(String filename) {
		try {
			return filename.substring(filename.lastIndexOf(".") + 1);
		} catch (Exception e) {
			return "";
		}
	}

	public static void output(Map<String, Integer> frequency) {
		Iterator<Entry<String, Integer>> iter = frequency.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, Integer> entry = iter.next();
			if (entry.getValue() > 1)
				System.out.println(entry.getKey() + ": " + entry.getValue());
		}
	}

	public static Map<String, Integer> frequency(String text, String commonWords) {
		// Loop through words in file
		String[] splitText = text.split("\\s+");
		Map<String, Integer> map = new HashMap<>();
		Integer n;
		String word = null;
		for (int i = 0; i < splitText.length; i++) {
			// If word NOT in most common list
			word = splitText[i].toLowerCase();
			if (!commonWords.contains(word)) {
				n = map.get(word);
				n = (n == null) ? 1 : ++n;
				map.put(word, n);
			}
		}
		return map;
	}

	static String stripTextLine(String line) {
		StringBuilder builder = new StringBuilder();

		// Remove all html tags
		line = line.replaceAll("\\<.*?>", "");

		String[] words = line.split(" ");
		for (int i = 0; i < words.length; i++) {
			// Discard any non-character character and change to lower case
			builder.append(words[i].replaceAll("\\W", "").toLowerCase() + " ");
		}
		return builder.toString();
		// return line.replaceAll("[^a-zA-Z0-9]", " ");
	}

	static String stripStartTag(String line) {
		String[] str = line.split(">");
		if (str.length == 2)
			return str[1];
		else
			return "";
	}

	static String stripEndTag(String line) {
		String[] str = line.split("<");
		return str[0];
	}

	static final int READING_BLOOMBERG = 1;
	static final int READING_TELEGRAPH = 2;
	static final int READING_FTC = 3;

	static String extractText(String filename) {

		StringBuilder text = new StringBuilder();

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF-8"));

			String line = reader.readLine();

			int textReading = 0;

			while (line != null) {

				// Extract Date
				if (line.contains("itemprop='datePublished'")) {
					String[] str1 = line.split("datetime='");
					String[] str2 = str1[1].split("T");
					Date date;
					Date compare;
					try {
						date = (new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)).parse(str2[0]);
						compare = (new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)).parse("2015-10-03");	// TODO: edit here
						System.out.println((date.getTime() - compare.getTime()) / (1000 * 86400));
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				// Extract headlines
				if (line.contains("class='headline'") || line.contains("itemprop=\"headline name\"")
						|| line.contains("itemprop=\"alternativeHeadline description\"") || line.contains("page-title")
						|| line.contains("field-subtitle")) {
					// Headline Bloomberg & Telegraph

					line = stripStartTag(line);

					while (!line.contains("<")) {
						line += reader.readLine();
					}
					line = stripEndTag(line);

					text.append(stripTextLine(line));
				}

				// Detect end of main content
				if (textReading > 0) {
					if (textReading == TextCleaner.READING_BLOOMBERG && line.contains("</div>")) {
						textReading = 0;
						break;
					}
					if (textReading == TextCleaner.READING_TELEGRAPH && line.contains("<div class=\"cl\"></div>")) {
						textReading = 0;
						text.append(stripTextLine(line)); // Telegraph has the
															// content in one
															// single line
						break;
					}
					if (textReading == TextCleaner.READING_FTC && line.contains("")) {
						textReading = 0;
						break;
					}
					text.append(stripTextLine(line));
				}

				// Detect starts of main content
				if (line.contains("id='article_body'")) {
					// Text starts from now
					textReading = TextCleaner.READING_BLOOMBERG;
				}
				if (line.contains("itemprop=\"articleBody\"")) {
					// Text starts from now
					textReading = TextCleaner.READING_TELEGRAPH;
				}
				if (line.contains("content:encoded")) {
					// Text starts from now
					textReading = TextCleaner.READING_FTC;
				}
				line = reader.readLine();
			}

			reader.close();

		} catch (FileNotFoundException e) {
			System.err.println("There is no file: " + filename);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return text.toString();
	}

}
