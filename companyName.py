import sys
from csv import reader

filename = sys.argv[1]
names = []
csv = reader(open(filename, 'r'), delimiter=';')
words = ['Inc.', 'Inc', 'Co.', 'Corp.', 'Corporation', ',', 'Holdings', 'Ltd.', 'Ltd', '(Hldg. )']
for row in csv:
	names.append(row[2])
del csv
for name in names:
	for w in words:
		name = name.replace(w, '')
	print(name)